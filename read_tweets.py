"""
Set of functions used to read in a series of tweets (pre-filtered to remove emojis),
into a list of lists, where each tweet is described by a list of the words contained.

Henry Day-Hall
Marley Samways
"""


# import os
import copy
import csv
import random
from nltk.stem.snowball import SnowballStemmer


def get_tweeter(path):
    """ Given a file path string decide what the tweeter is called.

    Expects the tweets to be organised in a dir named acording to the author
    and that these author-directorys are subdirectories of a dir called tweets.

    Parameters
    ----------
    path : string
        file path to the tweet

    Returns
    -------
    tweeter : string
        name of the tweeter
    """
    dir_string = "tweets/"
    tweeter = path[path.find(dir_string) + len(dir_string):
                   path.find("/", path.find(dir_string) + len(dir_string))]
    return tweeter


def find_authors(all_files):
    """ Using the format of the file path determing the name of the tweeter.

    Parameters
    ----------
    all_files : list of strings
        The file paths to the tweets.

    Returns
    -------
    tweeters_dict : dictionary of {string : int}
        The keys are the names of the tweeters and the int it the ID number
        assigned to them
    authors : list of int
        The id number of the author corrisponding to the path at the same
        position on the input list of files

    """
    author_dict = {}
    tweeter_ID = 0
    authors = []
    for path in all_files:
        tweeter = get_tweeter(path)
        if tweeter not in author_dict:
            author_dict[tweeter] = tweeter_ID
            tweeter_ID += 1
        authors.append(author_dict[tweeter])
    return author_dict, authors


def list_from_tweets(files):
    """
    Read all tweet files into a list of lists, where each tweet is represented
    as a list of the words it contains
    
    Parameters
    ----------
    files : list
        List of file names containing the tweets
    
    Returns
    -------
    tweets : list
        List of lists containing the words in each tweet for all tweets
    """
    tweets = [[] for filename in files]
    for i, filename in enumerate(files):
        with open(filename, "r") as f:
            # Read in all words for each tweet
            for line in f.readlines():
                for word in line.split():
                    tweets[i].append(word)
    return tweets


def preprocess_words(words, namesFileName, stopFileName):
    """
    Preprocess a 2d list of words.
    The preprocessor will delete all stopwords, then move all recognised names
    to a separate list. Then all the remaining no name words are stemmed.

    Parameters
    ----------
    words : list
        List of lists containing all raw words in each example
    namesFileName : str
        The file-name of the csv file containg human names to filter.
        Delimited by a space.
    stopFileName : str
        The file-name of the csv file containg stop files to delete.
        Delimited by a space.
    
    Returns
    -------
    processed words : list
        Set of processed words for each example.
    
    """
    # Start by extracting names because we don't want to stem them
    # In the same loop we can remove stop words
    with open(namesFileName, 'rt') as namesFile:
        namesFileReader = csv.reader(namesFile, delimiter = " ")
        for row in namesFileReader:
            possibleNames = set(row[:-1])
    with open(stopFileName, 'rt') as stopFile:
        stopFileReader = csv.reader(stopFile, delimiter = " ")
        for row in stopFileReader:
            stopWords = set(row)
    # Should be 1 if we want to stem and 0 if we dont
    stemMask = [[1] * len(line) for line in words]
    processed_words = copy.deepcopy(words)
    for lineN, line in enumerate(words):
        newWordN = 0
        for word in line:
            if word in possibleNames:
                stemMask[lineN][newWordN] = 0
                newWordN+=1                
                continue
            if(word[0] == '@'or word[0] == '#'):
                stemMask[lineN][newWordN] = 0
                newWordN+=1                
                continue
            if str.lower(word) in stopWords:
                del processed_words[lineN][newWordN]
                continue
            newWordN+=1
    stemmer = SnowballStemmer("english")
    for lineN, (line, maskLine) in enumerate(zip(processed_words, stemMask)):
        for wordN, (word, shouldStem) in enumerate(zip(line, maskLine)):
            if shouldStem:
                processed_words[lineN][wordN] = stemmer.stem(word)
    return processed_words


def make_numerical(all_tweets):
    """
    Convert each tweet from a list of words to a numerical vector, by assigning
    a number to each word

    Parameters
    ----------
    all_tweets : list
        List of lists containing all processed words in all tweets

    Returns
    -------
    tweet_vectors : list
        List of lists where each word in all_tweets has been replaced with a number
    word_dict : dict
        Dictionary giving the numerical ID for each word
    """
    word_dict = {}  # Dictionary to assign a numerical ID to each word seen
    for tweet in all_tweets:
        for word in tweet:
            if word not in word_dict.keys():
                word_dict[word] = len(word_dict.keys())
    # Convert each tweet to a vector, where the words have been converted to numbers
    tweet_vectors = []
    for tweet in all_tweets:
        vector = []
        for word in tweet:
            vector.append(word_dict[word])
        tweet_vectors.append(vector)
    return tweet_vectors, word_dict


def count_words(tweet_vectors, n_words):
    """
    Convert each vectorised tweet into a new vector, where each entry
    corresponds to the number of observations of each word

    Parameters
    ----------
    tweet_vectors : list
        Vectorised tweets, where the words have been converted to numbers
    n_words : int
        Total number of unique words observed in all tweets

    Returns
    -------
    all_counts : list
        List of lists, where each tweet is represented in terms of the number
        of times each word is contained
    """
    all_counts = []  # Counts of each word for all tweets
    for tweet in tweet_vectors:
        tweet_counts = [0] * n_words  # Counts of each word for this tweet
        # Count the number of times each word is seen
        for word in tweet:
            tweet_counts[word] += 1
        all_counts.append(tweet_counts)
    return all_counts


def process_all(files):
    """
    Process all tweets supplied in separate files, into numerical vectors
    describing the number of observations of each word.

    Parameters
    ----------
    files : list
        List of file names containing each of the tweets
    author : list
        List of 1s & 0s, indicating whether each tweet was written by the author of interest

    Returns
    -------
    histogrammed_tweets : list
        List of lists, where each sub-list contains the number of observations of each word
        in each tweet, after shuffling
    author : list
        Same as input author, but shuffled to keep in line with the shuffled tweets
    word_dict : dict
        Dictionary containing the numerical ID of each word encountered
    """
    # Read all tweets into a list of lists
    raw_tweets = list_from_tweets(files)
    # create a corrisponding list of authors
    author_dict, author_readOrder = find_authors(files)
    # Process the words contained in each tweet
    processed_tweets = preprocess_words(raw_tweets, "DatabaseOfNames.csv",
                                        "stop-word-list.csv")
    # Shuffle the tweets, making sure to shuffle the author label equivalently
    comb = list(zip(processed_tweets, author_readOrder))
    random.shuffle(comb)
    processed_tweets, author = zip(*comb)
    # Assign a numerical ID to each word and convert each tweet to a vector in this form
    vectorised_tweets, word_dict = make_numerical(processed_tweets)
    # Create a histogram for each tweet containing the number of instances of each word
    histogrammed_tweets = count_words(vectorised_tweets, len(word_dict))
    return histogrammed_tweets, author, word_dict, author_dict


if __name__ == "__main__":
    # Print error message if this script is ran alone
    print("This file is intended to act as a module only! Quitting.")
    quit()
