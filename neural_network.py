"""
Neural network for processing tweets.
Functions and classes required at the top. Script at the bottom.

Marley Samways
Henry Day-Hall
"""
import copy
from sys import argv  # For getting file paths with wildcards
import read_tweets
import torch
import torch.nn as nn
from torch.autograd import Variable


#########################################################
#
# FUNCTION DECLARATIONS
#
#########################################################

class Net(nn.Module):
    """ A 'deep' neural net with one hidden layer"""
    def __init__(self, input_size, hidden_size, num_classes):
        """Initilzation for the net. Creates the layer that will be used.

        Parameters
        ----------
        input_size : int
            the number of nodes at the input layer. This must equal the number
            of variables in each data point.
        hidden_size : int
            number of neurons in the hidden layer
        num_classes : int
            number of nodes at the output layer. This must equal the number of
            classes we wish to sort the data into.

        """
        super(Net, self).__init__()
        # create a linear fully connected layer
        self.fc1 = nn.Linear(input_size, hidden_size)
        # create a ReLU activation function
        self.relu = nn.ReLU()
        # create anouther linear fully connected layer
        self.fc2 = nn.Linear(hidden_size, num_classes)

    def forward(self, x):
        """Defines the actions for a forward pass through the net.

        Parameters
        ----------
        x : torch.FloatTensor
            the input variables from ths data point

        Returns
        -------
        out : torch.FloatTensor
            the values of the output nodes
        """
        # begin by passing the data to a linear fully connected layer,
        # this si the hidden layer
        out = self.fc1(x)
        # apply the activation function at the hidden layer
        out = self.relu(out)
        # move onto the output layer, which is also linear and fully connected
        out = self.fc2(out)
        return out


def mse_loss(output, target):
    """ A loss function to measure the performance of a net against its targets

    Parameters
    ----------
    output : torch.FloatTensor
        The output of the net
    target : torch.FloatTensor
        The ideal output

    Returns
    -------
    : torch.FloatTensor
        the sum squared distance
    """
    return torch.sum((output - target)**2)


def train(net, criterion, optimiser, num_epochs, histogrammed_tweets, authors):
    """ Train the net give it good weights and biases for the target.

    Parameters
    ----------
    net : Net
        The net to be trained. Modifed in place.
    criterion : function
        The loss function. This is the penalty perscribed when the output is
        not the target.
    optimiser : torch.optim
        The optimiser that will adjust the weights to minimize the criterion
    num_epochs : int
        The number of epochs to train for
    histogrammed_tweets : 2d list of ints
        The training data to be used. The top level list contains each data
        point. The data points themselves are lists of ints, each one giveing
        the frequency of the a word in the tweet.
    authors : list of int
        The list of targts for the training data

    """
    # the data is lumped into batches.
    # The optimiser will look at one batch at a time.
    batch_size = 5
    author_batches = []
    author_nextBatch = []
    tweet_batches = []
    tweet_nextBatch = []
    for i in range(len(authors)):
        author_nextBatch.append(authors[i])
        tweet_nextBatch.append(histogrammed_tweets[i])
        # check if we have reached the end of the batch
        if ((i+1) % batch_size) == 0:
            # add it to the list of batches
            author_batches.append(author_nextBatch)
            tweet_batches.append(tweet_nextBatch)
            # start a new batch
            author_nextBatch = []
            tweet_nextBatch = []
    # if there are any items left over put them in a partial batch at the end
    if len(author_nextBatch) != 0:
        author_batches.append(author_nextBatch)
        tweet_batches.append(tweet_nextBatch)
    # Start working through the training epochs
    # each epoch goes over every data point
    for epoch in range(num_epochs):
        for i, (tweet_batch, author_batch)\
                in enumerate(zip(tweet_batches, author_batches)):
            # Convert to a torch tensor Variable
            # torch tensor is required so the net understands it
            # variable is required so that it ban be diffrentiated in the
            # .backwards step
            tweet_batch = Variable(torch.FloatTensor(tweet_batch),
                                   requires_grad=True)
            # create a list of zeros with the right shape for the target
            targets = [[0] * num_classes for obs in tweet_batch]
            # put 1 into the position in the target vector that corrisponds to
            # the correct author
            for target, label in zip(targets, author_batch):
                target[label] = 1
            targets = Variable(torch.FloatTensor(targets), requires_grad=True)
            # reset the optimiser
            optimiser.zero_grad()  # zero the gradient buffer
            # forward pass
            outputs = net(tweet_batch)
            # find loss
            loss = criterion(outputs, targets)
            # backwards pass
            loss.backward()
            # optimise weights according to gradient found by backpropigation
            optimiser.step()
        # Print progress
        if (epoch+1) % 5 == 0:
            print('Epoch [%d/%d], Step [%d/%d], Loss: %.4f'
                  % (epoch + 1, num_epochs, i + 1,
                     len(histogrammed_tweets) // batch_size, loss.data[0]))


def evaluate_accuracy(net, test_tweets, test_authors, author_dict):
    """ A function to print the performance of the net on a test set of tweets

    Parameters
    ----------
    net : Net
        the net to be evaluated
    test_tweets : 2d list of ints
        The test data to be used. The top level list contains each data
        point. The data points themselves are lists of ints, each one giveing
        the frequency of the a word in the tweet.
    test_authors : list of int
        The list of targts for the test data. Each number is a author ID
    author_dict : dictionary of {str:int}
        a dictionary linking the author names (keys) to their
        ID numbers (vaules)

    """
    # Dictionary that contains {"author name": num tweets correctly identified}
    correct = author_dict.fromkeys(author_dict, 0)
    # Dictionary that contains {"author name": total num tweets}
    total = copy.deepcopy(correct)
    # inversion of the author_dict, so we can find a name from an ID
    ID_dict = {v: k for k, v in author_dict.items()}
    # go through the test data and check if it is correctly classified
    for obs, label in zip(test_tweets, test_authors):
        author_name = ID_dict[label]
        obs = Variable(torch.Tensor(obs))
        outputs = net(obs)
        _, predicted = torch.max(outputs.data, 0)
        total[author_name] += 1
        correct[author_name] += (predicted == label).sum()
    for author in author_dict:
        percent = 100. * float(correct[author])/float(total[author])
        print("Of the %d tweets by %s, %d were correctly identified."
              % (total[author], author, correct[author]))
        print("~~~~~~~~~~~~ %d%% match for %s ~~~~~~~~~~~~~"
              % (percent, author))


#########################################################
#
# SCRIPT
#
#########################################################

# read in command line arguments in a way that expands wildcards
all_files = argv[1:]; # needs a semicolon else its chatty
# do the preprocessing
(histogrammed_tweets, authors, word_dict,
 author_dict) = read_tweets.process_all(all_files)
# decide how much of the data to use as training data, and split it
fraction_to_train_on = 0.8
num_to_train_on = int(0.8 * len(histogrammed_tweets))
training_tweets = histogrammed_tweets[:num_to_train_on]
training_authors = authors[:num_to_train_on]
test_tweets = histogrammed_tweets[-num_to_train_on:]
test_authors = authors[-num_to_train_on:]
# parameters of the net
input_size = len(word_dict)
hidden_size = 200
num_classes = len(author_dict)
# and the traning process
num_epochs = 20
# Make the net
net = Net(input_size, hidden_size, num_classes)
# make the criterion that we will train agianst
criterion = mse_loss
# make the optimiser
learning_rate = 0.001
optimiser = torch.optim.Adam(net.parameters(), lr=learning_rate)
# Train the Model
train(net, criterion, optimiser, num_epochs, histogrammed_tweets, authors)

# Check how well it did
evaluate_accuracy(net, test_tweets, test_authors, author_dict)
